/**
 * Given two input strings, write a function to find largest identical substring
 * For example:
 * String
 * Spring
 * Here the largest substring is 'ring'
 *
 * In case there are multiple largest substrings of similar length, the function must return the first largest substring found w.r.t first string, For example:
 * hirespring
 * ringfhire
 * Here, the largest substring returned will be 'hire' and NOT 'ring'
*/

function largestSubstring(str1, str2) {
	var commonSubstrings = [''];
	for (var i = 0; i < str1.length; i++) {
		for (var j = 0; j < str2.length; j++) {
			if (str1[i] === str2[j]) {
				commonSubstrings.push(commmonSubstringAt(str1, str2, i, j));
			}
		}
	}
	return commonSubstrings.reduce(function(a, b) {
		return a.length >= b.length ? a : b;
	});
}

function commmonSubstringAt(str1, str2, i, j) {
	var start = i;
	while (i++ < str1.length && j++ < str2.length && str1[i] === str2[j])
	{}
	return str1.slice(start, i);
}