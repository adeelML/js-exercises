/*
 *  Write a JavaScript program to validate a date in dd/mm/yyyy format. If the user input matches with
 *  the format, the program will return a message "Valid Date" otherwise return a message "Invalid Date!"
 */

var checkDate = function(dateStr) {
	var dateParts = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(dateStr);
	return dateParts && dateParts[1] <= 31 && dateParts[2] <= 12 && notIvalidLeapDay(+dateParts[1], +dateParts[2], +dateParts[3]) ? "Valid Date" : "Invalid Date!";
};

function notIvalidLeapDay(d, m, y) {
	if (m === 2 && d === 29) {
		return y % 400 === 0 || (y % 4 === 0 && y % 100 !== 0);
	}
	return true;
}