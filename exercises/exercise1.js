/*
 * Write a function findLongestWord() that takes an array of words and returns the length of the longest word.
 *
 */

var findLongestWord = function(words) {
	return words.reduce(function(prev, curr) { return Math.max(prev, curr.length); }, 0);
};
