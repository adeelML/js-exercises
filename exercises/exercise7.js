/*
 *  Write a JavaScript function that accepts a string as a parameter and converts the first letter
 *  of each word of the string in upper case.
 */

var upperCase = function(str) {
	var strChars = str.split('');
	for (var i = 0, inWord = false; i < strChars.length; i++) {
		if (/\w/.test(strChars[i]) && !inWord) {
			strChars[i] = strChars[i].toUpperCase();
			inWord = true;
		}
		else if (/\s/.test(strChars[i])) {
			inWord = false;
		}
	}
	return strChars.join('');
};
