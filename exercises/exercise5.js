/*
 *  Write a JavaScript function that checks whether a passed string is palindrome or not.
 */

var isPalindrome = function(str) {
  return str === str.split('').reverse().join('');
};
