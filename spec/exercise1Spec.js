
describe("findLongestWord", function() {

  it("with an empty array", function() {
    expect(findLongestWord([])).toBe(0);
  });

  it("with the largest word being 4 letters", function() {
    expect(findLongestWord(['a', 'as', 'fds', 'asas', 'a', 'as'])).toBe(4);
  });

  it("with two largest words being the same size", function() {
    expect(findLongestWord(['as', 'asas', 'dswd', 'as', 'a'])).toBe(4);
  });

  // added tests
  it("with a single-element array", function() {
    expect(findLongestWord(['abc'])).toBe(3);
  });

  it("with array of empty strings", function() {
    expect(findLongestWord(['', ''])).toBe(0);
  });

});
