describe("isPalidrome", function() {

	it("with an empty string", function() {
		expect(isPalindrome("")).toBe(true);
	});

	it("with single word palindrome", function() {
		expect(isPalindrome("racecar")).toBe(true);
	});

	it("with multi word palindrome", function() {
		expect(isPalindrome("was it a car or a cat i saw")).toBe(false);
	});

	it("with multi word palindrome and mix caps", function() {
		expect(isPalindrome("Was it a car or a cat I saw")).toBe(false);
	});

	it("with invalid palindrome", function() {
		expect(isPalindrome("dady")).toBe(false);
	});

	// added tests
	it("with numeric characters", function() {
		expect(isPalindrome("11211")).toBe(true);
	});
	it("with numeric characters", function() {
		expect(isPalindrome("112")).toBe(false);
	});
	it("with numeric characters and alphabets and spaces", function() {
		expect(isPalindrome("42 aba 24")).toBe(true);
	});
	it("with special characters", function() {
		expect(isPalindrome("**$**")).toBe(true);
	});
	it("with special characters", function() {
		expect(isPalindrome("**$*")).toBe(false);
	});


});
